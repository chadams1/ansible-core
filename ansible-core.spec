# We need this because we are no longer noarch, since our bundled deps might
# conceivably need to compile arch-specific things. But we currently have no
# useful debuginfo stuff.
%global debug_package %{nil}

# Disable shebang munging for specific paths.  These files are data files.
# ansible-test munges the shebangs itself.
%global __brp_mangle_shebangs_exclude_from_file %{SOURCE1}

# Filter requires on jinja2 since bundled
%global __requires_exclude jinja2

%global commitId ed924d64935b15341731544af6a03b55e9739741
%global python39_sitelib /usr/lib/python3.9/site-packages/

# RHEL and Fedora add -s to the shebang line.  We do *not* use -s -E -S or -I
# with ansible because it has many optional features which users need to
# install libraries on their own to use.  For instance, paramiko for the
# network connection plugins or winrm to talk to windows hosts.
# Set this to nil to remove -s
%define py_shbang_opts %{nil}
%define py2_shbang_opts %{nil}
%define py3_shbang_opts %{nil}

%define vendor_path %{buildroot}%{python39_sitelib}/ansible/_vendor/
%define vendor_pip /usr/bin/python3.9 -m pip install --no-deps -v --no-use-pep517 --no-binary :all: -t %{vendor_path}

# These control which bundled dep versions we pin against
%global jinja2_version 3.1.2
%global markupsafe_version 2.1.0
%global packaging_version 20.4
%global pyparsing_version 2.4.7
%global straightplugin_version 1.4.1


Name: ansible-core
Summary: SSH-based configuration management, deployment, and task execution system
Version: 2.14.1
Release: 1%{?dist}

Group: Development/Libraries
License: GPLv3+
Source0: ansible-%{commitId}.tar.gz
Source1: ansible-test-data-files.txt

# And bundled deps
Source2: jinja2-b08cd4bc64bb980df86ed2876978ae5735572280.tar.gz
Source3: markupsafe-b5a517506d6cb8091e215a4a89e47db5eee6a68f.tar.gz
Source4: packaging-ded06cedf6e20680eea0363fac894cb4a09e7831.tar.gz
Source5: pyparsing-6a844ee35ca5125490a28dbd6dd2d15b6498e605.tar.gz

# Deps to build man pages
Source6: straightplugin-6634ea8e1e89d5bb23804f50e676f196c52c46ed.tar.gz

URL: http://ansible.com

# We obsolete old ansible, and any version of ansible-base.
Obsoletes: ansible < 2.10.0
Obsoletes: ansible-base

# ... and provide 'ansible' so that old packages still work without updated
# spec files.
# Provides: ansible

# Bundled provides that are sprinkled throughout the codebase.
Provides: bundled(python-backports-ssl_match_hostname) = 3.7.0.1
Provides: bundled(python-distro) = 1.6.0
Provides: bundled(python-selectors2) = 1.1.1
Provides: bundled(python-six) = 1.16.0

# Things we explicitly bundle via src rpm, and put in ansible._vendor
Provides: bundled(python-jinja2) = %{jinja2_version}
Provides: bundled(python-markupsafe) = %{markupsafe_version}
Provides: bundled(python-packaging) = %{packaging_version}
Provides: bundled(python-pyparsing) = %{pyparsing_version}
Provides: bundled(python-straightplugin) = %{straightplugin_version}

BuildRequires: python3-devel
BuildRequires: python3-docutils
BuildRequires: python3-pip
BuildRequires: python3-pyyaml
BuildRequires: python3-resolvelib
BuildRequires: python3-rpm-macros
BuildRequires: python3-setuptools
BuildRequires: python3-wheel
BuildRequires: make git-core gcc

Requires: git-core
Requires: python3
Requires: python3-PyYAML >= 5.1
Requires: python3-cryptography
Requires: python3-resolvelib >= 0.5.3
Requires: python3-six
Requires: sshpass

%description
Ansible is a radically simple model-driven configuration management,
multi-node deployment, and remote task execution system. Ansible works
over SSH and does not require any software or daemons to be installed
on remote nodes. Extension modules can be written in any language and
are transferred to managed machines automatically.

%package -n ansible-test
Summary: Tool for testing ansible plugin and module code
Requires: %{name} = %{version}-%{release}

%description -n ansible-test
Ansible is a radically simple model-driven configuration management,
multi-node deployment, and remote task execution system. Ansible works
over SSH and does not require any software or daemons to be installed
on remote nodes. Extension modules can be written in any language and
are transferred to managed machines automatically.

This package installs the ansible-test command for testing modules and plugins
developed for ansible.

%prep
%setup -q -T -b 2 -n jinja2-b08cd4bc64bb980df86ed2876978ae5735572280
%setup -q -T -b 3 -n markupsafe-b5a517506d6cb8091e215a4a89e47db5eee6a68f
%setup -q -T -b 4 -n packaging-ded06cedf6e20680eea0363fac894cb4a09e7831
%setup -q -T -b 5 -n pyparsing-6a844ee35ca5125490a28dbd6dd2d15b6498e605
%setup -q -T -b 6 -n straightplugin-6634ea8e1e89d5bb23804f50e676f196c52c46ed
%setup -q -n ansible-%{commitId}

%build
/usr/bin/python3.9 setup.py build

%install
/usr/bin/python3.9 setup.py install --root %{buildroot}

# Handle bundled deps:
%{vendor_pip} \
  ../jinja2-b08cd4bc64bb980df86ed2876978ae5735572280/ \
  ../markupsafe-b5a517506d6cb8091e215a4a89e47db5eee6a68f/ \
  ../packaging-ded06cedf6e20680eea0363fac894cb4a09e7831/ \
  ../pyparsing-6a844ee35ca5125490a28dbd6dd2d15b6498e605/

# Create system directories that Ansible defines as default locations in
# ansible/config/base.yml
DATADIR_LOCATIONS='%{_datadir}/ansible/collections
%{_datadir}/ansible/plugins/doc_fragments
%{_datadir}/ansible/plugins/action
%{_datadir}/ansible/plugins/become
%{_datadir}/ansible/plugins/cache
%{_datadir}/ansible/plugins/callback
%{_datadir}/ansible/plugins/cliconf
%{_datadir}/ansible/plugins/connection
%{_datadir}/ansible/plugins/filter
%{_datadir}/ansible/plugins/httpapi
%{_datadir}/ansible/plugins/inventory
%{_datadir}/ansible/plugins/lookup
%{_datadir}/ansible/plugins/modules
%{_datadir}/ansible/plugins/module_utils
%{_datadir}/ansible/plugins/netconf
%{_datadir}/ansible/roles
%{_datadir}/ansible/plugins/strategy
%{_datadir}/ansible/plugins/terminal
%{_datadir}/ansible/plugins/test
%{_datadir}/ansible/plugins/vars'

UPSTREAM_DATADIR_LOCATIONS=$(grep -ri default lib/ansible/config/base.yml | tr ':' '\n' | grep '/usr/share/ansible')

if [ "$SYSTEM_LOCATIONS" != "$UPSTREAM_SYSTEM_LOCATIONS" ] ; then
	echo "The upstream Ansible datadir locations have changed.  Spec file needs to be updated"
	exit 1
fi

mkdir -p %{buildroot}%{_datadir}/ansible/plugins/
for location in $DATADIR_LOCATIONS ; do
	mkdir %{buildroot}"$location"
done
mkdir -p %{buildroot}%{_sysconfdir}/ansible/
mkdir -p %{buildroot}%{_sysconfdir}/ansible/roles/

cp examples/hosts %{buildroot}%{_sysconfdir}/ansible/
cp examples/ansible.cfg %{buildroot}%{_sysconfdir}/ansible/
mkdir -p %{buildroot}/%{_mandir}/man1/
# Build man pages

mkdir /tmp/_vendor
/usr/bin/python3.9 -m pip install ../straightplugin-6634ea8e1e89d5bb23804f50e676f196c52c46ed -t /tmp/_vendor --no-build-isolation

# Remove plugins not needed, they bring in more dependencies
find hacking/build_library/build_ansible/command_plugins ! -name 'generate_man.py' -type f -exec rm -f {} +

PYTHON=python3.9 PYTHONPATH=%{vendor_path}:/tmp/_vendor make docs
cp -v docs/man/man1/*.1 %{buildroot}/%{_mandir}/man1/

cp -pr docs/docsite/rst .
cp -p lib/ansible_core.egg-info/PKG-INFO .

%files
%defattr(-,root,root)
%{_bindir}/ansible*
%exclude %{_bindir}/ansible-test
%config(noreplace) %{_sysconfdir}/ansible/
%doc README.rst PKG-INFO COPYING
%doc changelogs/CHANGELOG-v2.*.rst
%doc %{_mandir}/man1/ansible*
%{_datadir}/ansible/
%{python39_sitelib}/ansible*
%exclude %{python39_sitelib}/ansible_test
%exclude %{python39_sitelib}/ansible/_vendor/markupsafe/_speedups.c

%files -n ansible-test
%{_bindir}/ansible-test
%{python39_sitelib}/ansible_test

%changelog
* Wed Dec 07 2022 Dimitri Savineau <dsavinea@redhat.com> - 2.14.1-1
- ansible-core 2.14.1 release (rhbz#2151593)

* Tue Nov 08 2022 Dimitri Savineau <dsavinea@redhat.com> - 2.14.0-1
- ansible-core 2.14.0 release (rhbz#2141116)

* Mon Nov 07 2022 Dimitri Savineau <dsavinea@redhat.com> - 2.13.6-1
- ansible-core 2.13.6 release (rhbz#2140778)
- fix service_facts module parsing (rhbz#2128801)

* Tue Oct 11 2022 James Marshall <jamarsha@redhat.com> - 2.13.5-1
- ansible-core 2.13.5 release (rhbz#2133912)

* Thu Oct 06 2022 Dimitri Savineau <dsavinea@redhat.com> - 2.13.4-1
- ansible-core 2.13.4 release (rhbz#2132807)

* Mon Aug 15 2022 James Marshall <jamarsha@redhat.com> - 2.13.3-1
- ansible-core 2.13.3 release (rhbz#2118458)

* Mon Jul 18 2022 James Marshall <jamarsha@redhat.com> - 2.13.2-1
- ansible-core 2.13.2 release (rhbz#2108229)

* Mon Jun 27 2022 Dimitri Savineau <dsavinea@redhat.com> - 2.13.1-2
- Update bundled jinja2 version to 3.1.2 (rhbz#2101462)

* Wed Jun 22 2022 Dimitri Savineau <dsavinea@redhat.com> - 2.13.1-1
- ansible-core 2.13.1 release (rhbz#2100242)
- add bundled version of jinja2 and markupsafe

* Mon Jun 20 2022 Dimitri Savineau <dsavinea@redhat.com> - 2.12.7-1
- ansible-core 2.12.7 release (rhbz#2099317)
- remove legacy nightly configuration

* Tue May 24 2022 James Marshall <jamarsha@redhat.com> - 2.12.6-1
- ansible-core 2.12.6 release

* Fri May 13 2022 Dimitri Savineau <dsavinea@redhat.com> - 2.12.5-2
- switch from git to git-core dependency (rhbz#2083386)

* Mon May 09 2022 Dimitri Savineau <dsavinea@redhat.com> - 2.12.5-1
- ansible-core 2.12.5 release

* Wed Apr 06 2022 James Marshall <jamarsha@redhat.com> - 2.12.4-1
- ansible-core 2.12.4 release

* Mon Mar 14 2022 Dimitri Savineau <dsavinea@redhat.com> - 2.12.3-1
- ansible-core 2.12.3 release

* Tue Feb 01 2022 Dimitri Savineau <dsavinea@redhat.com> - 2.12.2-1
- ansible-core 2.12.2 release

* Tue Dec 07 2021 James Marshall <jamarsha@redhat.com> - 2.12.1-1
- ansible-core 2.12.1-1

* Mon Nov 08 2021 Dimitri Savineau <dsavinea@redhat.com> - 2.12.0-1
- ansible-core 2.12.0-1

* Tue Oct 12 2021 Christian Adams <chadams@redhat.com> - 2.11.6-1
- ansible-core 2.11.6-1, fix CVE-2021-3620, ansible-connection module
  no long discloses sensitive info.

* Wed Oct 06 2021 Yanis Guenane <yguenane@redhat.com> - 2.11.5-3
- ansible-core 2.11.5-3, add virtual provide for straightplugin

* Wed Sep 15 2021 Josh Boyer <jwboyer@redhat.com> - 2.11.5-2
- ansible-core 2.11.5-2

* Mon Sep 13 2021 Josh Boyer <jwboyer@redhat.com> - 2.11.3-3
- Bump for build

* Wed Jul 21 2021 Paul Belanger <pabelanger@redhat.com> - 2.11.3-2
- Add git dependency for ansible-galaxy CLI command.

* Tue Jul 20 2021 Yanis Guenane <yguenane@redhat.com> - 2.11.3-1
- ansible-core 2.11.3-1

* Fri Jul 02 2021 Satoe Imaishi <simaishi@redhat.com> - 2.11.2-2
- Add man pages

* Tue Jun 29 2021 Paul Belanger <pabelanger@redhat.com> - 2.11.2-1
- ansible-core 2.11.2 released.
- Drop bundled version of resolvelib in favor of
  python38-resolvelib.

* Wed Mar 31 2021 Rick Elrod <relrod@redhat.com> - 2.11.0b4-1
- ansible-core 2.11.0 beta 4

* Thu Mar 18 2021 Rick Elrod <relrod@redhat.com> - 2.11.0b2-3
- Try adding a Provides for old ansible.

* Thu Mar 18 2021 Rick Elrod <relrod@redhat.com> - 2.11.0b2-2
- Try Obsoletes instead of Conflicts.

* Thu Mar 18 2021 Rick Elrod <relrod@redhat.com> - 2.11.0b2-1
- ansible-core 2.11.0 beta 2
- Conflict with old ansible and ansible-base.

* Thu Mar 11 2021 Rick Elrod <relrod@redhat.com> - 2.11.0b1-1
- ansible-core 2.11.0 beta 1

* Mon Nov 30 2020 Rick Elrod <relrod@redhat.com> - 2.11.0-1
- ansible-core, beta

* Wed Jun 10 2020 Rick Elrod <relrod@redhat.com> - 2.10.0-1
- ansible-base, beta
